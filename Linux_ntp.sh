#作者：随缘
#交流群：619838534
#!/bin/bash

# 检查是否是root用户
if [[ $EUID -ne 0 ]]; then
   echo "请使用root用户或使用sudo命令执行此脚本。" 
   exit 1
fi

# 检查系统版本
if [[ -f /etc/debian_version ]]; then
    # Debian/Ubuntu
    apt-get update
    apt-get install -y sudo
    sudo apt-get install -y ntp
    sudo timedatectl set-timezone UTC
	#sudo ntpdate -u ntp.aliyun.com time.windows.com time.apple.com time.google.com time.cloudflare.com
	#sudo hwclock --systohc # 强制写入硬件
elif [[ -f /etc/redhat-release ]]; then
    # CentOS/RHEL
    yum update
    yum install -y sudo
    sudo yum install -y ntp
    sudo timedatectl set-timezone UTC
	#sudo ntpdate -u ntp.aliyun.com time.windows.com time.apple.com time.google.com time.cloudflare.com
	#sudo hwclock --systohc # 强制写入硬件
else
    echo "无法识别的操作系统。"
    exit 1
fi

echo ""
echo -e "----------------------------------------------------"
echo -e "作者：随缘"
echo -e "QQ交流群①：695963494"
echo -e "QQ交流群②：619838534"
echo -e "云星博客：blog.ggapi.cn"
echo -e "----------------------------------------------------"
echo -e "时间同步已开启。"
exit 0
